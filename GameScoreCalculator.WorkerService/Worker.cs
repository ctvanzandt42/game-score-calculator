using System.Threading;
using System.Threading.Tasks;
using GameScoreCalculator.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace GameScoreCalculator.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly string _apiKey;
        private readonly string _emailHost;
        private readonly string _emailPassword;
        private readonly int _emailPort;
        private readonly string _emailRecipient;
        private readonly string _emailSender;
        private readonly IEmailSenderService _emailService;
        private readonly IGameScoreCalculationService _gameScoreService;

        public Worker(IConfiguration configuration,
            IGameScoreCalculationService gameScoreService, IEmailSenderService emailService)
        {
            _apiKey = configuration["API:ApiKey"];
            _emailSender = configuration["Email:EmailSender"];
            _emailPassword = configuration["Email:EmailPassword"];
            _emailRecipient = configuration["Email:EmailRecipient"];
            _emailHost = configuration["Email:EmailHost"];
            _emailPort = int.Parse(configuration["Email:EmailPort"]);
            _gameScoreService = gameScoreService;
            _emailService = emailService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _emailService.SendEmailWithResults(_emailHost,
                    _emailPort,
                    _emailSender,
                    _emailPassword,
                    _emailRecipient,
                    await _gameScoreService.CalculateMaxGameScoreAsync(_apiKey));

                await Task.Delay(86400000, stoppingToken);
            }
        }
    }
}