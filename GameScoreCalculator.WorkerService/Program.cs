using GameScoreCalculator.Service;
using GameScoreCalculator.Service.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GameScoreCalculator.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddTransient<IGameRetrievalService, GameRetrievalService>();
                    services.AddTransient<IStatisticsRetrievalService, StatisticsRetrievalService>();
                    services.AddTransient<IPlayerRetrievalService, PlayerRetrievalService>();
                    services.AddTransient<IGameScoreCalculationService, GameScoreCalculationService>();
                    services.AddTransient<IEmailSenderService, EmailSenderService>();
                    services.AddHostedService<Worker>();
                });
    }
}