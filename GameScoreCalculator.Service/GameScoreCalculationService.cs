using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameScoreCalculator.Domain.StatisticsByGame;
using GameScoreCalculator.Service.Interfaces;
using MoreLinq.Extensions;


namespace GameScoreCalculator.Service
{
    public class GameScoreCalculationService : IGameScoreCalculationService
    {
        private readonly IGameRetrievalService _gameRetrievalService;
        private readonly IStatisticsRetrievalService _statisticsRetrievalService;
        private readonly IPlayerRetrievalService _playerRetrievalService;

        public GameScoreCalculationService(IGameRetrievalService gameRetrievalService,
            IStatisticsRetrievalService statisticsRetrievalService, IPlayerRetrievalService playerRetrievalService)
        {
            _gameRetrievalService = gameRetrievalService;
            _statisticsRetrievalService = statisticsRetrievalService;
            _playerRetrievalService = playerRetrievalService;
        }

        public async Task<string> CalculateMaxGameScoreAsync(string apiKey)
        {
            var stats = new List<List<Statistics>>();
            var yesterdayGames = await _gameRetrievalService.GetGamesFromYesterdayAsync(apiKey);
            
            if (yesterdayGames.Count == 0) return "No games were played yesterday!";
            
            var gameIds = yesterdayGames.Select(game => game.GameId).ToList();

            foreach (var gameId in gameIds)
            {
                var statsDto = await _statisticsRetrievalService.GetAllStatsPerGameAsync(gameId, apiKey);
                stats.Add(statsDto);
            }

            var highestGameScoreObj = stats.SelectMany(stat => stat).MaxBy(statistic => statistic.CalculateGameScore());
            var playerId = highestGameScoreObj.ToList()[0].PlayerId;
            var gameScore = highestGameScoreObj.ToList()[0].CalculateGameScore();

            var playerName = await _playerRetrievalService.GetPlayerByIdAsync(playerId, apiKey);
            return $"The highest game score from yesterday's games was {playerName} with a game score of {gameScore}";
        }
    }
}