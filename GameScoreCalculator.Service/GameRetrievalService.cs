using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameScoreCalculator.Domain.GamesByDate;
using GameScoreCalculator.Service.Interfaces;
using Newtonsoft.Json;
using RestSharp;

using static GameScoreCalculator.Service.RequestCreation;

namespace GameScoreCalculator.Service
{
    public sealed class GameRetrievalService : IGameRetrievalService
    {
        public async Task<List<Game>> GetGamesFromYesterdayAsync(string apiKey)
        {
            var dateString = DateTime.Today.AddDays(-1).ToUniversalTime().ToString("yyyy-MM-dd");
            var client = new RestClient($"https://api-nba-v1.p.rapidapi.com/games/date/{dateString}");
            var response = await client.ExecuteTaskAsync(CreateRestRequest(apiKey));

            var responseDto = JsonConvert.DeserializeObject<GamesByDateResponse>(response.Content);

            return responseDto.Api.Games;
        }
    }
}