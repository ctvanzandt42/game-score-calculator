using System.Collections.Generic;
using System.Threading.Tasks;
using GameScoreCalculator.Domain.StatisticsByGame;
using GameScoreCalculator.Service.Interfaces;
using Newtonsoft.Json;
using RestSharp;

using static GameScoreCalculator.Service.RequestCreation;

namespace GameScoreCalculator.Service
{
    public sealed class StatisticsRetrievalService : IStatisticsRetrievalService
    {
        public async Task<List<Statistics>> GetAllStatsPerGameAsync(string gameId, string apiKey)
        {
            var client = new RestClient($"https://api-nba-v1.p.rapidapi.com/statistics/players/gameId/{gameId}");
            var response = await client.ExecuteTaskAsync(CreateRestRequest(apiKey));

            var responseDto = JsonConvert.DeserializeObject<StatsByGameResponse>(response.Content);

            return responseDto.Api.Statistics;
        }
    }
}