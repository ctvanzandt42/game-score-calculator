using System.Threading.Tasks;

namespace GameScoreCalculator.Service.Interfaces
{
    public interface IEmailSenderService
    {
        void SendEmailWithResults(string emailHost,
            int emailPort,
            string emailSender,
            string emailPassword,
            string emailRecipient,
            string emailBody);
    }
}