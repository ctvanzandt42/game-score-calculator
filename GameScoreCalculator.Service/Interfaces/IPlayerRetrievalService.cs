using System.Threading.Tasks;

namespace GameScoreCalculator.Service.Interfaces
{
    public interface IPlayerRetrievalService
    {
        Task<string> GetPlayerByIdAsync(string playerId, string apiKey);
    }
}