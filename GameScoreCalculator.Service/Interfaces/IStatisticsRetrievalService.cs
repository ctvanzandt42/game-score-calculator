using System.Collections.Generic;
using System.Threading.Tasks;
using GameScoreCalculator.Domain.StatisticsByGame;

namespace GameScoreCalculator.Service.Interfaces
{
    public interface IStatisticsRetrievalService
    {
        Task<List<Statistics>> GetAllStatsPerGameAsync(string gameId, string apiKey);
    }
}