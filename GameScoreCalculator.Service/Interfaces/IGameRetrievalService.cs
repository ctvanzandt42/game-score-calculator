using System.Collections.Generic;
using System.Threading.Tasks;
using GameScoreCalculator.Domain.GamesByDate;

namespace GameScoreCalculator.Service.Interfaces
{
    public interface IGameRetrievalService
    {
        Task<List<Game>> GetGamesFromYesterdayAsync(string apiKey);
    }
}