using System.Threading.Tasks;

namespace GameScoreCalculator.Service.Interfaces
{
    public interface IGameScoreCalculationService
    {
        Task<string> CalculateMaxGameScoreAsync(string apiKey);
    }
}