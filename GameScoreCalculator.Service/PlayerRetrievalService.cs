using System.Threading.Tasks;
using GameScoreCalculator.Domain.PlayerByPlayerId;
using GameScoreCalculator.Service.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using static GameScoreCalculator.Service.RequestCreation;

namespace GameScoreCalculator.Service
{
    public sealed class PlayerRetrievalService : IPlayerRetrievalService
    {
        public async Task<string> GetPlayerByIdAsync(string playerId, string apiKey)
        {
            var client = new RestClient($"https://api-nba-v1.p.rapidapi.com/players/playerId/{playerId}");
            var response = await client.ExecuteTaskAsync(CreateRestRequest(apiKey));

            var responseDto = JsonConvert.DeserializeObject<PlayerByPlayerIdResponse>(response.Content);

            return $"{responseDto.Api.Players[0].FirstName} {responseDto.Api.Players[0].LastName}";
        }
    }
}