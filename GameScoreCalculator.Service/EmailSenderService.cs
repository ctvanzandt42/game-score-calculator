using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using GameScoreCalculator.Service.Interfaces;

namespace GameScoreCalculator.Service
{
    public sealed class EmailSenderService : IEmailSenderService
    {
        public void SendEmailWithResults(string emailHost, int emailPort, string emailSender, string emailPassword,
            string emailRecipient, string emailBody)
        {
            var smtpClient = new SmtpClient(emailHost)
            {
                Port = emailPort,
                Credentials = new NetworkCredential(emailSender, emailPassword),
                EnableSsl = true,
            };
            
            smtpClient.Send(emailSender,
                emailRecipient,
                "Game Score Calculator",
                emailBody);
        }
    }
}