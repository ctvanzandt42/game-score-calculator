using RestSharp;

namespace GameScoreCalculator.Service
{
    public sealed class RequestCreation
    {
        public static RestRequest CreateRestRequest(string apiKey)
        {
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "api-nba-v1.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", apiKey);

            return request;
        }
    }
}