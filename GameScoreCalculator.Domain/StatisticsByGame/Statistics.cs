using System;
using System.Runtime.CompilerServices;

namespace GameScoreCalculator.Domain.StatisticsByGame
{
    public sealed class Statistics
    {
        public string Points { get; set; }
        public string Fgm { get; set; }
        public string Fga { get; set; }
        public string Ftm { get; set; }
        public string Fta { get; set; }
        public string OffReb { get; set; }
        public string DefReb { get; set; }
        public string Assists { get; set; }
        public string PFouls { get; set; }
        public string Steals { get; set; }
        public string Turnovers { get; set; }
        public string Blocks { get; set; }
        public string PlayerId { get; set; }

        public double CalculateGameScore()
        {
            if (!int.TryParse(Points, out var points))
                points = 0;
            if (!int.TryParse(Fgm, out var fgm))
                fgm = 0;
            if (!int.TryParse(Fga, out var fga))
                fga = 0;
            if (!int.TryParse(Fta, out var fta))
                fta = 0;
            if (!int.TryParse(Ftm, out var ftm))
                ftm = 0;
            if (!int.TryParse(OffReb, out var offReb))
                offReb = 0;
            if (!int.TryParse(DefReb, out var defReb))
                defReb = 0;
            if (!int.TryParse(Steals, out var steals))
                steals = 0;
            if (!int.TryParse(Assists, out var assists))
                assists = 0;
            if (!int.TryParse(Blocks, out var blocks))
                blocks = 0;
            if (!int.TryParse(PFouls, out var pFouls))
                pFouls = 0;
            if (!int.TryParse(Turnovers, out var turnovers))
                turnovers = 0;
                
            return points
                   + (0.4 * fgm)
                      - (0.7 * fga)
                      - (0.4
                         * (fta - ftm))
                      + (0.7 * offReb)
                      + (0.3 * defReb)
                      + steals
                      + (0.7 * assists)
                      + (0.7 * blocks)
                      - 0.4 * pFouls
                      - turnovers;
        }
    }
}