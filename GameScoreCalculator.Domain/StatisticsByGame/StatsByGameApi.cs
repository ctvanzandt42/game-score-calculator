using System.Collections.Generic;

namespace GameScoreCalculator.Domain.StatisticsByGame
{
    public sealed class StatsByGameApi
    {
        public List<Statistics> Statistics { get; set; }
    }
}