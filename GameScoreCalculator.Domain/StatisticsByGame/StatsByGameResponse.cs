namespace GameScoreCalculator.Domain.StatisticsByGame
{
    public sealed class StatsByGameResponse
    {
        public StatsByGameApi Api { get; set; }
    }
}