using System.Collections.Generic;

namespace GameScoreCalculator.Domain.GamesByDate
{
    public sealed class GamesByDateApi
    {
        public List<Game> Games { get; set; }
    }
}