using GameScoreCalculator.Domain.GamesByDate;

namespace GameScoreCalculator.Domain.GamesByDate
{
    public sealed class GamesByDateResponse
    {
        public GamesByDateApi Api { get; set; }
    }
}