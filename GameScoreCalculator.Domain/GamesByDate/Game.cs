namespace GameScoreCalculator.Domain.GamesByDate
{
    public sealed class Game
    {
        public string GameId { get; set; }
    }
}