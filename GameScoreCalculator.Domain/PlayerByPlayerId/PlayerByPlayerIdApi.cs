using System.Collections.Generic;

namespace GameScoreCalculator.Domain.PlayerByPlayerId
{
    public sealed class PlayerByPlayerIdApi
    {
        public List<Player> Players { get; set; }
    }
}