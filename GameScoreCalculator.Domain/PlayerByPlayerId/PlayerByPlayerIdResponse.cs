namespace GameScoreCalculator.Domain.PlayerByPlayerId
{
    public sealed class PlayerByPlayerIdResponse
    {
        public PlayerByPlayerIdApi Api { get; set; }
    }
}