namespace GameScoreCalculator.Domain.PlayerByPlayerId
{
    public sealed class Player
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}