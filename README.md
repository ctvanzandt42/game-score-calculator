<h1>Game Score Calculator</h1>

<p>An opportunity to experiment with .NET Core 3.1's Worker Service template. This project will run similarly to a cron job; once per day, it will call an API to get all information from the previous day's NBA games. It will calculate the game score of each player from those games, and send an email recapping the top performers from the day before. Game Score is an advanced statistic meant to calculate total impact on a single game by a player.</p>

<h3>Technologies Used</h3>
<ul>
    <li>.NET Core 3.1</li>
</ul>